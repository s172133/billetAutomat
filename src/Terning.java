import java.util.ArrayList;

public class Terning {
	/** antallet af øjne på den side på terningen, der vender opad lige nu */
	private int værdi;

	/** konstruktør der opretter en terning */
	public Terning()
	{
		// vælg en tilfældig side til at starte med
		værdi = (int) (Math.random() * 6 + 1);
	}

	/** kaster terningen, så den får en anden værdi */
	public void kast()
	{
		// vælg en tilfældig side
		double tilfældigtTal = Math.random();
		værdi = (int) (tilfældigtTal * 6 + 1);
	}

	/** Aflæser terningens værdi */
	public int getVærdi()
	{
		return værdi;
	}

	/** Sætter terningens værdi */
	public void setVærdi(int nyVærdi)
	{
		værdi = nyVærdi;
	}

	ArrayList<Terning> terninger;

	public void Dice(int antalTerninger)
	{
		terninger = new ArrayList<Terning>();
		for (int i=0; i<antalTerninger; i++)
		{
			Terning t = new Terning();
			terninger.add(t);
		}
	}

}
