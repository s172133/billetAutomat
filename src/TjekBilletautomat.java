
import java.util.Scanner;

public class TjekBilletautomat {

	/*
			Er ikke nået så langt med opgaven, så der er en del fejl der
			gør at systemet ikke køre.
	testrettelse

	*/



	/*
	Errors:	reused variables names
			duplicated error checks "værdi != xx"
			no price scanner
			no quantity setup
			no refund system
			print of ticket was placed in the middle of the program
			balance is reset after print
			montørkode bliver ikke "anvendt"
	*/

	public static void main(String[] arg) {
		int antalFejl = 0;                   // opsætning af testen
		Billetautomat automat = new Billetautomat(10); // default ticket
		int udestaaende =  automat.getSamletSalgsbeløb("1234") - automat.getBalance();
		Scanner scan = new Scanner(System.in);
			System.out.println(	"Vælg billet:\n" +
								"45 kroner billet\t - 1\n" +
								"35 kroner billet\t - 2\n" +
								"25 kroner billet\t - 3\n" +
								"10 kroner billet\t - 4\n" +
								"Afslut køb\t\t\t - 5\n");
		int menu = scan.nextInt();
		while(!(menu <= 5 && menu >=1)){
			System.out.println("Indtastningen er ugyldig, prøv igen\n");
			menu = scan.nextInt();
		}
		switch (menu){
			case 1:
				automat.setBilletpris("1234",45);
				break;
			case 2:
				automat.setBilletpris("1234",35);
				break;
			case 3:
				automat.setBilletpris("1234",25);
				break;
			case 4:
				automat.setBilletpris("1234",10);
				break;
			default:
				break;
		}

		System.out.println("Hvor mange billetter ønsker de?\n");
		int antalbilletter = scan.nextInt();



		System.out.println("Indsæt beløb\n");
		int beløb = scan.nextInt(); // scan efter beløb
		while(beløb < 0){			// hvis beløb er negativ, prøv igen
			System.out.println("Indtastningen er ugyldig, prøv igen\n");
			beløb = scan.nextInt();
		}

		automat.indsætPenge(beløb);             // afprøvning af indsætPenge()
		while(automat.getBalance() != udestaaende){
			System.out.println(automat.getBalance());
			System.out.println("Vi mangler "+ udestaaende + " kr. til billet prisen\n");

			System.out.println(	"indsæt nyt beløb?\n" +
								"Ja  \t - 1\n" +
								"Nej \t - 2\n");
			int valg = scan.nextInt();
			if(valg != 1){
				break;
			} else {
				beløb += scan.nextInt();
				automat.indsætPenge(beløb);     // afprøvning af indsætPenge()
			}
		}

		System.out.println(automat.getBalance());

		int antalSolgteBilletter = automat.getSamletSalgsbeløb("1234"); // tjek af forventet tilstand


		automat.udskrivBillet();             // afprøvning af udskrivBillet()



		if (udestaaende != 0) {
			System.out.println("FEJL, getBalance() giver ikke 0, men " + udestaaende);
			antalFejl = antalFejl + 1;
		}

		if (antalFejl == 0) {
			System.out.println("Automaten er fejlfri, HURRA!!!");
		} else {
			System.out.println("Der blev fundet " + antalFejl + " fejl i automaten, ØV!");
		}
	}
}
