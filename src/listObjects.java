import java.util.ArrayList;
import java.util.Scanner;

public class listObjects {

	public static void main(String[] args) {

		Terning terning = new Terning();
		Scanner scan = new Scanner(System.in);

		// indtast antal terninger
		System.out.println("Enter amount of dice: ");
		int numDice = scan.nextInt();
		while(numDice < 0){
			System.out.println("Please enter a valid amount");
			numDice = scan.nextInt();
		}


		// indtast antal 6'er vi skal ramme
		System.out.println("Enter number of sixes which should occur: ");
		int sixOccur = scan.nextInt();
		while(sixOccur < 0){
			System.out.println("Please enter a valid amount");
			sixOccur = scan.nextInt();
		}


		// Slå nu med terningen indtil vi får en isSix
		boolean isSix = false;
		int[] diceArray = new int[numDice];
		int countThrows = 0;
		int countSix;

		// while loop, så længe vi ikke har opnået antal 6'er
		while (!isSix)
		{
			countSix = 0; // initiate count
			countThrows = countThrows + 1;


			// fortag kast og indsæt i array
			for (int i = 0; i < numDice; i++) {
				terning.kast();
				diceArray[i] = terning.getVærdi();
			}


			// output runde med værdier
			System.out.print("Round: "+ countThrows + "; ");
			for (int i = 0; i < numDice; i++) {
				System.out.print(" - "+diceArray[i]);
			}

			System.out.println("\n");


			// tæl antal 6'er i vores array
			for (int i = 0; i < numDice ; i++) {
				if (diceArray[i] == 6) {
					countSix++;
				}
				if (countSix >= sixOccur) isSix = true; // hvis vi rammer antal 6'er
			}

		}

		System.out.println("We got " + sixOccur + " sixes after " + countThrows + " Throws.");
	}
}


